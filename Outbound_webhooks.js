
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
const axios = require("axios");

//The data and the ID of the deal that will be updated
const data = { 
    select:["ID","NAME"]
};

//Call the API crm.deal.update to update the particulat deal

axios
  .post("https://dev-eze24.ezecomcorp.com/rest/1/hzndlbno5oq7aftm/crm.activity.type.list/",data)
  .then(res => {
    //Logs the data on the console
    console.log(res.data);
  }).catch(err=>{
    //Logs the error on the console
    console.log(err);
  });
